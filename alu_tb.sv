module alu_tb;

//------------------------------------------------------------------------------
// type and variable definitions
//------------------------------------------------------------------------------
	typedef enum bit [2:0]{
		AND = 3'b000,
		OR = 3'b001,
		ADD = 3'b100,
		SUB = 3'b101
	} opcode_t;

	typedef enum bit [1:0] {
		DATA = 2'b00,
		CTL = 2'b01
	} frame_t;

	typedef enum bit [1:0] {
		ERR_DATA,
		ERR_CRC,
		ERR_OP,
		NO_ERR
	} error_t;

	typedef struct packed {
		bit C;
		bit Ov;
		bit Z;
		bit N;
	} flags_st;

	typedef struct packed {
		bit [31:0] B;
		bit [31:0] A;
		bit err;
		bit [2:0] op_bit;
		bit [3:0] crc;
	} sindata_st;

	typedef struct packed {
		bit [31:0] C;
		bit err;
		bit [6:0] Ctl_bits;
	} soutdata_st;

	bit clk;
	bit rst_n;
	bit sin = 1'b1;
	wire sout;


//------------------------------------------------------------------------------
// DUT instantiation
//------------------------------------------------------------------------------
	mtm_Alu u_mtm_Alu (
		.clk  (clk), //posedge active clock
		.rst_n(rst_n), //synchronous reset active low
		.sin  (sin), //serial data input
		.sout (sout) //serial data output
	);

//------------------------------------------------------------------------------
// Coverage block
//------------------------------------------------------------------------------


	bit [31:0] A_cov;
	bit [31:0] B_cov;
	opcode_t opcode_cov;
	error_t error_cov;

	covergroup op_err_cov;

		option.name = "cg_op_err_cov";

		coverpoint opcode_cov {
			// #A1 test all operations
			bins A1_all_ops[] = {[AND:SUB]};

			// #A2 two operations in row
			bins A2_two_ops[] = ([AND:SUB] [* 2]);
		}
		coverpoint error_cov {
			// #B1 test all errors
			bins B1_all_errors[] = {[ERR_DATA:ERR_OP], NO_ERR};

			// #B2 test all errors after no error
			bins B2_all_no_err[] = (NO_ERR => [ERR_DATA:ERR_OP]);

			// #B3 test all errors after no error
			bins B3_no_all_err[] = ([ERR_DATA:ERR_OP] => NO_ERR);

			// #B4 test two errors in row
			bins B4_twoerr[] = ([ERR_DATA:ERR_OP] [* 2]);

			// #B5 test two no errors in row
			bins B5_twonoerr[] = ( NO_ERR [* 2]);
		}

	endgroup

	covergroup zeros_or_ones_on_ops;

		option.name = "cg_zeros_or_ones_on_ops";

		all_ops : coverpoint opcode_cov {
			bins ops = {[AND:SUB]};
		}

		a_leg: coverpoint A_cov {
			bins zeros = {'h0000_0000};
			bins others= {['h0000_0001:'hFFFF_FFFE]};
			bins ones  = {'hFFFF_FFFF};
		}

		b_leg: coverpoint B_cov {
			bins zeros = {'h0000_0000};
			bins others= {['h0000_0001:'hFFFF_FFFE]};
			bins ones  = {'hFFFF_FFFF};
		}

		B_op_00_FF:  cross a_leg, b_leg, all_ops {

			// #B1 simulate all zero input for all the operations

			bins B1_and_00 = binsof (all_ops) intersect {AND} &&
			(binsof (a_leg.zeros) || binsof (b_leg.zeros));

			bins B1_or_00 = binsof (all_ops) intersect {OR} &&
			(binsof (a_leg.zeros) || binsof (b_leg.zeros));

			bins B1_add_00 = binsof (all_ops) intersect {ADD} &&
			(binsof (a_leg.zeros) || binsof (b_leg.zeros));

			bins B1_sub_00 = binsof (all_ops) intersect {SUB} &&
			(binsof (a_leg.zeros) || binsof (b_leg.zeros));

			// #B2 simulate all one input for all the operations

			bins B2_and_FF = binsof (all_ops) intersect {AND} &&
			(binsof (a_leg.ones) || binsof (b_leg.ones));

			bins B2_or_FF = binsof (all_ops) intersect {OR} &&
			(binsof (a_leg.ones) || binsof (b_leg.ones));

			bins B2_add_FF = binsof (all_ops) intersect {ADD} &&
			(binsof (a_leg.ones) || binsof (b_leg.ones));

			bins B2_sub_FF = binsof (all_ops) intersect {SUB} &&
			(binsof (a_leg.ones) || binsof (b_leg.ones));

			ignore_bins others_only =
			binsof(a_leg.others) && binsof(b_leg.others);
		}
	endgroup

	op_err_cov oc;
	zeros_or_ones_on_ops c_00_FF;

	initial begin :coverage
		oc = new();
		c_00_FF = new();

		forever begin : sample_cov
			read_sin(A_cov, B_cov, opcode_cov, error_cov);
			oc.sample();
			c_00_FF.sample();
		end
	end :coverage

	task automatic read_sin(
			output bit [31:0] A,
			output bit [31:0] B,
			output opcode_t opcode,
			output error_t error
		);
		bit [7:0] data;
		sindata_st data_buff;
		frame_t frame_type;
		bit [3:0] frame_cntr;

		do begin
			read_sin_frame(frame_type,data);
			data_buff = {data_buff[63:0],data};
			frame_cntr++;
		end
		while(frame_type == DATA && frame_cntr < 9);

		if(frame_cntr != 9 || frame_type == DATA) begin
			A = 0;
			B = 0;
			opcode = AND;
			error = ERR_DATA;
		end
		else if(data_buff.crc != CRC4forData68({data_buff.B,data_buff.A,1'b1,data_buff.op_bit})) begin
			A = data_buff.A;
			B = data_buff.B;
			opcode = opcode_t'(data_buff.op_bit);
			error = ERR_CRC;
		end
		else if(data_buff.op_bit[1]) begin
			A = data_buff.A;
			B = data_buff.B;
			opcode = opcode_t'(data_buff.op_bit);
			error = ERR_OP;
		end
		else begin
			A = data_buff.A;
			B = data_buff.B;
			opcode = opcode_t'(data_buff.op_bit);
			error = NO_ERR;
		end
	endtask :read_sin

	task automatic read_sin_frame(
			output frame_t frame_type,
			output bit [7:0] data
		);
		int bit_cntr;
		begin
			while(sin != 1'b0) @(posedge clk);
			@(posedge clk)
				if(sin == 1'b0) begin :data_frame
					frame_type = DATA;
				end :data_frame
			else begin:ctl_frame
				frame_type = CTL;
			end:ctl_frame
			for(bit_cntr = 7; bit_cntr>=0; bit_cntr--)begin :read_data
				@(posedge clk) data[bit_cntr] = sin;
			end :read_data
			@(posedge clk);
		end
	endtask :read_sin_frame
//------------------------------------------------------------------------------
// Clock generator
//------------------------------------------------------------------------------

	initial begin :clk_genenerator
		clk = 0;
		forever begin : clk_frv
			#5;
			clk = ~clk;
		end
	end :clk_genenerator

//------------------------------------------------------------------------------
// Tester
//------------------------------------------------------------------------------
	initial begin: tester
		bit [31:0] A;
		bit [31:0] B;
		opcode_t opcode;
		error_t error_type;

		#20 rst_n = '1;
		#20;
		repeat(1000) begin :tester_loop
			A = rand_data();
			B = rand_data();
			opcode = rand_opcode();
			error_type = rand_error();

			send_cmd(A,B,opcode,error_type);
			#600;
		end :tester_loop

		#500
		$finish;
	end :tester

	task send_cmd(
			input bit [31:0] A,
			input bit [31:0] B,
			input opcode_t opcode,
			input error_t error_type
		);

		bit [2:0] op_bit;
		bit [3:0] CRC4_to_send;

		begin
			$cast(op_bit, opcode);

			case(error_type)
				ERR_DATA: begin
					CRC4_to_send = CRC4forData68({B,A,1'b1,op_bit});
					send_byte(B[7:0]  , DATA);
					send_byte({1'b0,op_bit,CRC4_to_send}, CTL);
				end
				ERR_OP: begin
					op_bit = op_bit + 3'b010;
					CRC4_to_send = CRC4forData68({B,A,1'b1,op_bit});
					send_byte(B[31:24], DATA);
					send_byte(B[23:16], DATA);
					send_byte(B[15:8] , DATA);
					send_byte(B[7:0]  , DATA);
					send_byte(A[31:24], DATA);
					send_byte(A[23:16], DATA);
					send_byte(A[15:8] , DATA);
					send_byte(A[7:0]  , DATA);
					send_byte({1'b0,op_bit,CRC4_to_send}, CTL);
				end
				ERR_CRC: begin
					CRC4_to_send = $random;
					send_byte(B[31:24], DATA);
					send_byte(B[23:16], DATA);
					send_byte(B[15:8] , DATA);
					send_byte(B[7:0]  , DATA);
					send_byte(A[31:24], DATA);
					send_byte(A[23:16], DATA);
					send_byte(A[15:8] , DATA);
					send_byte(A[7:0]  , DATA);
					send_byte({1'b0,op_bit,CRC4_to_send}, CTL);
				end
				NO_ERR: begin
					CRC4_to_send = CRC4forData68({B,A,1'b1,opcode});
					send_byte(B[31:24], DATA);
					send_byte(B[23:16], DATA);
					send_byte(B[15:8] , DATA);
					send_byte(B[7:0]  , DATA);
					send_byte(A[31:24], DATA);
					send_byte(A[23:16], DATA);
					send_byte(A[15:8] , DATA);
					send_byte(A[7:0]  , DATA);
					send_byte({1'b0,op_bit,CRC4_to_send}, CTL);
				end
			endcase
		end
	endtask :send_cmd

	task send_byte(
			input bit[7:0] data,
			input frame_t frame_type
		);

		int bit_cntr;
		bit [10:0] frame;
		begin
			frame = {frame_type,data,1'b1};
			for(bit_cntr = 10; bit_cntr >= 0; bit_cntr--) begin
				@(negedge clk)
					sin = frame[bit_cntr];
			end
		end
	endtask :send_byte

	function bit [3:0] CRC4forData68(
			input bit [67:0] Data_in
		);
		// polynomial: x^4 + x^1 +1 (0x9) with initial value 0
		bit [67:0] d;
		bit [3:0] Result;

		begin
			d = Data_in;
			Result[0] = d[66] ^ d[64] ^ d[63] ^ d[60] ^ d[56] ^ d[55] ^ d[54] ^ d[53] ^ d[51] ^ d[49] ^ d[48] ^ d[45] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[36] ^ d[34] ^ d[33] ^ d[30] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[19] ^ d[18] ^ d[15] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[4] ^ d[3] ^ d[0];
			Result[1] = d[67] ^ d[66] ^ d[65] ^ d[63] ^ d[61] ^ d[60] ^ d[57] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[48] ^ d[46] ^ d[45] ^ d[42] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[33] ^ d[31] ^ d[30] ^ d[27] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[18] ^ d[16] ^ d[15] ^ d[12] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[3] ^ d[1] ^ d[0];
			Result[2] = d[67] ^ d[66] ^ d[64] ^ d[62] ^ d[61] ^ d[58] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[49] ^ d[47] ^ d[46] ^ d[43] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[34] ^ d[32] ^ d[31] ^ d[28] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[17] ^ d[16] ^ d[13] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[4] ^ d[2] ^ d[1];
			Result[3] = d[67] ^ d[65] ^ d[63] ^ d[62] ^ d[59] ^ d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[50] ^ d[48] ^ d[47] ^ d[44] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[35] ^ d[33] ^ d[32] ^ d[29] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[18] ^ d[17] ^ d[14] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[3] ^ d[2];
			return Result;
		end
	endfunction : CRC4forData68

	function opcode_t rand_opcode();
		bit [1:0] op_choice;
		op_choice = $random;
		case (op_choice)
			2'b00: return AND;
			2'b01: return OR;
			2'b10: return ADD;
			2'b11: return SUB;
		endcase
	endfunction :rand_opcode

	function bit [31:0] rand_data();
		bit [1:0] corner_value;
		corner_value = $random;
		case (corner_value)
			2'b00: return 32'h0000_0000;
			2'b01: return $random;
			2'b10: return $random;
			2'b11: return 32'hFFFF_FFFF;
		endcase
	endfunction :rand_data

	function error_t rand_error();
		bit [2:0] err_choice;
		err_choice = $random;
		case (err_choice)
			3'b000: return ERR_DATA;
			3'b001: return ERR_OP;
			3'b010: return ERR_CRC;
			3'b011: return ERR_DATA;
			default: return NO_ERR;
		endcase
	endfunction :rand_error
//------------------------------------------------------------------------------
// Scoreboard
//------------------------------------------------------------------------------
	bit [68:0] sin_q[$];
	int log_file;
	bit Not_passed;

	initial begin : scoreboard_init
		log_file = $fopen("sim_log", "w");
	end

	always begin :score_read_sin
		bit [31:0] A;
		bit [31:0] B;
		opcode_t opcode;
		error_t error;

		read_sin(A, B, opcode, error);

		sin_q.push_front({A,B,opcode,error});
	end :score_read_sin

	always @* begin : scoreboard
		bit [31:0] A;
		bit [31:0] B;
		opcode_t opcode;
		error_t error_sin;
		bit [31:0] C;
		bit [2:0] crc_sout;
		error_t error_sout;
		flags_st flags;

		bit [31:0] predicted_result;
		flags_st predicted_flags;
		bit [2:0] predicted_crc;

		wait(rst_n == 1)
		#1

		read_sout(C, flags, crc_sout, error_sout);
		{A,B,opcode, error_sin} = sin_q.pop_back();
		#1;
		if(error_sin == NO_ERR && error_sout == NO_ERR) begin
			predicted_data(A,B,opcode,predicted_result,predicted_flags, predicted_crc);

			#1
			if (predicted_result != C || predicted_flags != flags || predicted_crc != crc_sout) begin
				$fwrite(log_file, "FAILED: ");
				Not_passed = 1;
			end
			else begin
				$fwrite(log_file, "PASSED: ");
			end
			$fwrite(log_file, "A: %8h  B: %8h  opcode: %3s C: %8h flags: %4b crc: %3b ---- Predicted -> C: %8h flags: %4b crc: %3b ---- Sim time: %9d[ps]\n", A, B, opcode.name(), C, flags, crc_sout, predicted_result, predicted_flags, predicted_crc, $time());

		end
		else if (error_sin != NO_ERR && error_sout == NO_ERR) begin
			$fwrite(log_file, "FAILED: Sent error frame and received result\n");
			Not_passed = 1;
		end
		else if (error_sin == NO_ERR && error_sout != NO_ERR) begin
			$fwrite(log_file, "FAILED: Sent correct frame and received error\n");

			Not_passed = 1;
		end
		else if (error_sin != NO_ERR && error_sout != NO_ERR) begin
			if (error_sin == error_sout) begin
				$fwrite(log_file, "PASSED: Error sent and error received: %8s \n", error_sout.name());
			end
			else begin
				$fwrite(log_file, "FAILED: Error sent: %8s ---- Error received: %8s\n", error_sin.name(), error_sout.name());
				Not_passed = 1;
			end

		end
	end : scoreboard

	final begin : scoreboard_summary
		if(Not_passed == 0)begin
			$fwrite(log_file,"\n\
                         #       ###################       _   _ \n\
                        #        #                 #       *   * \n\
                   #   #         #   TEST PASSED   #         |   \n\
                    # #          #                 #       \\___/\n\
                     #           ###################\n ");
			$display("\n\
                         #       ###################       _   _ \n\
                        #        #                 #       *   * \n\
                   #   #         #   TEST PASSED   #         |   \n\
                    # #          #                 #       \\___/\n\
                     #           ###################\n\n\
                         For detailed info see sim_log file.\n ");
		end
		else begin
			$fwrite(log_file,"\n\
                   #     #       ###################       _   _ \n\
                    #  #         #                 #       x   x \n\
                     #           #   TEST FAILED   #         |   \n\
                   #  #          #                 #        ___  \n\
                 #     #         ###################       /   \\\n ");
			$display("\n\
                   #     #       ###################       _   _ \n\
                    #  #         #                 #       x   x \n\
                     #           #   TEST FAILED   #         |   \n\
                   #  #          #                 #        ___  \n\
                 #     #         ###################       /   \\\n\n\
                         For detailed info see sim_log file.\n ");
		end
		$fclose(log_file);
	end : scoreboard_summary

	task automatic read_sout(
			output bit [31:0] C,
			output bit [3:0] flags,
			output bit [2:0] crc,
			error_t error
		);
		bit [7:0] data;
		soutdata_st data_buff;
		frame_t frame_type;
		int frame_cntr;

		do begin
			read_sout_frame(frame_type,data);
			data_buff = {data_buff[31:0],data};
			frame_cntr++;
		end
		while(frame_type == DATA && frame_cntr < 5);

		if(frame_cntr == 1 && data_buff.err == 1'b1) begin
			C = 0;
			flags = 0;
			crc = 0;
			case(data_buff.Ctl_bits[6:1])
				6'b100100: error = ERR_DATA;
				6'b010010: error = ERR_CRC;
				6'b001001: error = ERR_OP;
				default: error = ERR_DATA;
			endcase
		end
		else if(frame_cntr == 5) begin
			C = data_buff.C;
			flags = data_buff.Ctl_bits[6:3];
			crc = data_buff.Ctl_bits[2:0];
			error = NO_ERR;
		end
		else begin
			C = 0;
			flags = 0;
			crc = 0;
			error = NO_ERR;
		end
	endtask :read_sout

	task automatic read_sout_frame(
			output frame_t frame_type,
			output bit [7:0] data
		);
		int bit_cntr;
		begin
			while(sout != 1'b0) @(negedge clk);
			@(negedge clk)
				if(sout == 1'b0) begin :data_frame
					frame_type = DATA;
				end :data_frame
			else begin:ctl_frame
				frame_type = CTL;
			end:ctl_frame
			for(bit_cntr = 7; bit_cntr>=0; bit_cntr--)begin :read_data
				@(negedge clk) data[bit_cntr] = sout;
			end :read_data
			@(negedge clk);
		end
	endtask :read_sout_frame

	task automatic predicted_data(
			input bit [31:0] A,
			input bit [31:0] B,
			input opcode_t opcode,
			output bit [31:0] C,
			output flags_st flags,
			output bit [2:0] crc
		);
		begin
			case (opcode)
				AND: begin
					C = A & B;
					flags.C = 1'b0;
					flags.Ov = 1'b0;
					flags.N = C[31];
					flags.Z = ~(|C);
				end
				OR: begin
					C = A | B;
					flags.C = 1'b0;
					flags.Ov = 1'b0;
					flags.N = C[31];
					flags.Z = ~(|C);
				end
				ADD: begin
					{flags.C,C} = {1'b0,A} + B;
					flags.Ov = flags.C ^ (A[31]^(B[31]^C[31]));
					flags.N = C[31];
					flags.Z = ~(|C);
				end
				SUB: begin
					{flags.C, C} = {1'b0,B} - A;
					flags.Ov = flags.C ^ (A[31]^(B[31]^C[31]));
					flags.N = C[31];
					flags.Z = ~(|C);
				end
			endcase // case (op_set)

			crc = CRC3forData37({C,1'b0,flags});
		end
	endtask : predicted_data

	function bit [2:0] CRC3forData37(
			input bit [36:0] Data_in
		);
		// polynomial: x^3 + x^1 + 1 (0x5) with initial value 0
		bit [36:0] d;
		bit [2:0] Result;

		begin
			d = Data_in;
			Result[0] = d[35] ^ d[32] ^ d[31] ^ d[30] ^ d[28] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[18] ^ d[17] ^ d[16] ^ d[14] ^ d[11] ^ d[10] ^ d[9] ^ d[7] ^ d[4] ^ d[3] ^ d[2] ^ d[0];
			Result[1] = d[36] ^ d[35] ^ d[33] ^ d[30] ^ d[29] ^ d[28] ^ d[26] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[16] ^ d[15] ^ d[14] ^ d[12] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[2] ^ d[1] ^ d[0];
			Result[2] = d[36] ^ d[34] ^ d[31] ^ d[30] ^ d[29] ^ d[27] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[17] ^ d[16] ^ d[15] ^ d[13] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[3] ^ d[2] ^ d[1];
			return Result;
		end
	endfunction :CRC3forData37

endmodule : alu_tb
